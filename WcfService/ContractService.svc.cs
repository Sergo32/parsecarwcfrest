﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DbClassLibrary.Entities;
using DbClassLibrary;

namespace WcfService
{

    public class ContractService : IContractService
    {
     
        public List<Car> GetAllCars()
        {
            return DbManager.GetInstance().TableCar.GetAllCars();
        }

        public List<Car> GetAllCarsToPrice(int price)
        {
           return DbManager.GetInstance().TableCar.GetAllCarsToPrice(price);
        }

        public List<TopCars> GetAllTopCars()
        {
           
            return DbManager.GetInstance().TableTopCars.GetAllTopCars();
        }
     
        public List<Car> GetCars(TopCars topCars)
        {
          return  DbManager.GetInstance().TableCar.GetAllCarsIdMark(topCars.Id);
        }

    

       
    }
}
