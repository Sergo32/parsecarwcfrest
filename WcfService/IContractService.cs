﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DbClassLibrary.Entities;

namespace WcfService
{

    [ServiceContract]
    public interface IContractService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetCars", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Car> GetCars(TopCars topCars);

       
        [OperationContract]
        [WebGet(UriTemplate = "/GetAllCars", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Car> GetAllCars();

        [OperationContract]
        [WebGet(UriTemplate = "/GetAllTopCars", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<TopCars> GetAllTopCars();

        [OperationContract]
        [WebGet(UriTemplate = "/GetAllCarsToPrice?price={price}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Car> GetAllCarsToPrice(int price);


    }


    
}
