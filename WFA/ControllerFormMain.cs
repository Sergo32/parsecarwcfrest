﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbClassLibrary.Entities;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;

namespace WFA
{
    class ControllerFormMain
    {
        private FormMain form;
      
        public ControllerFormMain(FormMain form)
        {
            this.form = form;           
        }

        public void FillComboBoxTopCar()
        {
            string url = $"http://localhost:53444/ContractService.svc/rest/GetAllTopCars";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = webClient.DownloadString(url);
            List<TopCars> topCars = JsonConvert.DeserializeObject<List<TopCars>>(json);
       
            form.comboBoxTopCar.DataSource = null;
            form.comboBoxTopCar.DataSource = topCars;
        }
       
        public void ClearFields()
        {
            form.comboBoxTopCar.SelectedItem = -1;
        }

        public void GetCarsById()
        {
                    
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string url = "http://localhost:53444/ContractService.svc/rest/GetCars";

            client.Headers[HttpRequestHeader.ContentType] = "application/json";
    
            TopCars top = (TopCars)form.comboBoxTopCar.SelectedItem;
            string json = JsonConvert.SerializeObject(top);
            string responseJson = client.UploadString(url, "POST", json);                   
            List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(responseJson);
         
            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }

        public void GetAllCars()
        {
            string url = $"http://localhost:53444/ContractService.svc/rest/GetAllCars";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = webClient.DownloadString(url);
            List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(json);

            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
        }

        public void GetAllCarsToPrice()
        {
            if (form.maskedTextBoxPrice.Text == "")
            {
                MessageBox.Show("Введите значение");
                return;
            }
            int price = int.Parse(form.maskedTextBoxPrice.Text);
            string url = $"http://localhost:53444/ContractService.svc/rest/GetAllCarsToPrice?price={price}";
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string json = webClient.DownloadString(url);
            List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(json);
        
            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }


    }
}
