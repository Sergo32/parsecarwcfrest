﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WFA
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormMain(this);

            Thread.Sleep(3000);//компьютер не быстрый дал время на парсинг
            controller.FillComboBoxTopCar();
        }

        public Task GetCarsByIdAsync()
        {
            return Task.Factory.StartNew(() => Invoke((MethodInvoker)(() =>
            {
                controller.GetCarsById();
            })));
        }

        private async void buttonLoad_Click(object sender, EventArgs e)
        {
            await GetCarsByIdAsync();
        } 
     
        private async void buttonClear_Click(object sender, EventArgs e)
        {
            await GetAllCarsAsync();
        }

        public Task GetAllCarsAsync()
        {
            return Task.Factory.StartNew(() => Invoke((MethodInvoker)(() =>
            {
                controller.GetAllCars();
            })));
        }

        private async void buttonPrice_Click(object sender, EventArgs e)
        {
            await GetAllCarsToPriceAsync();
        }

        public Task GetAllCarsToPriceAsync()
        {
            return Task.Factory.StartNew(() => Invoke((MethodInvoker)(() =>
            {
                controller.GetAllCarsToPrice();
            })));
        }
    }
}
