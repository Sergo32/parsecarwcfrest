﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbClassLibrary.Entities
{
    public class Car
    {
        public int Id { get; set; }
        public int IdCarBrand { get; set; }
        public string NameCar { get; set; }
        public int Price { get; set; }


        public TopCars TopCars { get; set; }
    }
}
